function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	this.fight = function(target){
		console.log(target.name + "'s health is: " + target.health)
		console.log(this.name + ("'s tackle damage is: ") + this.attack)

		console.log("---Battle Starts Now! ---")

		this.thunderbolt = function(){
			target.currentHealth =  target.health - this.attack;
			target.health = target.currentHealth;
			console.log(this.name + " used thunderbolt")
			console.log(target.name + "'s health is now " + target.health)
			console.log("--It is supper effective!--")

			if(target.health < 5){
			target.faint()
			}
		}
	}
	
	this.faint = function(){
		console.warn(this.name + " fainted.")	
	};
}

let pikachu = new Pokemon('Pikachu', 16)
let charizard = new Pokemon('Charizard', 8)
console.log(pikachu)
console.log(charizard)

pikachu.fight(charizard)
pikachu.thunderbolt()
pikachu.thunderbolt()




